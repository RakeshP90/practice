import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { DataService } from './data.service';
// import { environment } from '../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ConfigService } from './apiconf.service';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  bearerToken: string = '';
  apiUrl: string= '';
  backendapiUrl:string='';
  constructor(private http: HttpClient, private dataService: DataService,private configService: ConfigService) {
    // setTimeout(() => {
    //   this.setapiUrl();
    // },0);searchValue

    // this.setapiUrl();
  }
  // setapiUrl() {
  //   return new Promise(resolve => {
  //     this.apiUrl = this.configService.getConfiguration().apiUrl;
  //     resolve(this.apiUrl);
  //   });
  // }

  setBasicToken() {
    return new Promise(resolve => {
      this.bearerToken = this.configService.getConfiguration().token;
      resolve(this.bearerToken);
    });
  }
  
  get(url) {
    if (!this.dataService.loading) {
      setTimeout(() => { this.dataService.loading = true; }, 10);
    }
    return this.http.get(this.backendapiUrl + url, this.appendHeaders(url)).pipe(
      map((respo) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        if (error.status == 401) {
        //   this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  post(url, data, fileUrl = false) {
    console.log('\n api this.apiUrl...', this.apiUrl);
    if (!this.dataService.loading) {
      setTimeout(() => { this.dataService.loading = true; }, 10);
    }
    return this.http.post(this.backendapiUrl + url, data, this.appendHeaders(url, fileUrl)).pipe(
      map((respo) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }

        if (error.status == 401) {
        //   this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  put(url, data, fileUrl = false) {
    if (!this.dataService.loading) {
      setTimeout(() => { this.dataService.loading = true; }, 10);
    }
    return this.http.put(this.backendapiUrl + url, data, this.appendHeaders(url, fileUrl)).pipe(
      map((respo) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        if (error.status == 401) {
        //   this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  delete(url) {
    if (!this.dataService.loading) {
      setTimeout(() => { this.dataService.loading = true; }, 10);
    }
    return this.http.delete(this.backendapiUrl + url, this.appendHeaders(url)).pipe(
      map((respo) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        return respo;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.dataService.loading) {
          setTimeout(() => { this.dataService.loading = false; }, 10);
        }
        if (error.status == 401) {
        //   this.dataService.logOutAdmin();
        }
        return throwError(error);
      }));
  }

  getNewApi(url, type, data = '') {
    if (type == 'get') {
      return this.http.get(url, this.appendHeaders(url));
    }
    if (type == 'post') {
      return this.http.post(url, data, this.appendHeaders(url));
    }
  }

  appendHeaders(url, fileUrl = false) {
    if (url == 'Token Url') {
      let headers = new HttpHeaders({ 'Content-Type': ("application/x-www-form-urlencoded") });
      headers = headers.append('Authorization', 'Basic '+this.bearerToken);
      // headers = headers.append('Authorization', 'Basic ZnBScUFCbWJ1Sm83eXgwOGZQcHZvT0prcGFVYTpJWXV0SFZkVFFhZWw3TUd3MjRHQ2FVQmtHQlFh');
      return { headers: headers };
    } else {
    //   let AccessToken = this.dataService.getAccessToken();
    //   if (AccessToken) {
    //     let headers;
    //     if (fileUrl) {
    //       headers = new HttpHeaders();
    //     } else {
    //       headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    //     }
    //     headers = headers.append('Authorization', ("Bearer " + AccessToken));
    //     return { headers: headers };
    //   } else {
    //     this.dataService.logOutAdmin();
    //   }
    }
  }
}
