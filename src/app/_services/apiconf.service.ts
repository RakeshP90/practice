import {Configuration} from "../_models/config.model";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ConfigService 
{
private config:Configuration;

constructor(private http:HttpClient){}
load(url: string) {
    return new Promise((resolve) => {
        this.http.get(url).subscribe((config: Configuration) => {
            this.config = config;
            resolve(config);
        });
    });
}

getConfiguration():Configuration
{
    return this.config;
}

}