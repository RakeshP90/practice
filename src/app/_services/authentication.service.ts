import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import { User } from '../_models/user';
// import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
private currentUserSubject:BehaviorSubject<User>;
public currentUser:Observable<User>;
  constructor(private http:HttpClient) { 
    this.currentUserSubject=new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
this.currentUser=this.currentUserSubject.asObservable();
 }
 public get currentUserValue():User
 {
   return this.currentUserSubject.value;
 }

login(requestData)
{
  console.log(JSON.stringify(requestData))
  return this.http.post<any>(`${environment.backendapiUrl}api/users/authenticate`,requestData)
  .pipe(map((user) =>{
    console.log(user)
    if(user.userData) {
     const userexisted = user.userData;
      localStorage.setItem('currentUser',JSON.stringify(userexisted.authToken));
      this.currentUserSubject.next(user.userData);
    }  
    return user.userData;
  }));
}

logout()
{
  localStorage.removeItem('currentUser');
  this.currentUserSubject.next(null);
}



}
