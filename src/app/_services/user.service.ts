import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../_models/user';

@Injectable({providedIn:'root'})
export class UserService
{
    constructor(private http:HttpClient){}

getAll()
{
    return this.http.get<User[]>(`${environment.backendapiUrl}api/users`);
}

register(data)
{
    console.log('-------user registration data--------' + JSON.stringify(data))
    return this.http.post<any>(`${environment.backendapiUrl}api/users/register`,data);
}

// getMultiSelect(){
//     return this.http.get<any>(`${environment.backendapiUrl}api/multiselect`);
// }

}