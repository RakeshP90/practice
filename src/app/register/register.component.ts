import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../_services/authentication.service';
import { UserService } from '../_services/user.service';
// import { first } from 'rxjs/operators';
import { MustMatch } from '../_helper/mustmatch.component';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerform:FormGroup;
  loading=false;
  isSubmitted=false;
  error:string;

  constructor(private toastr: ToastrService,private formBuilder:FormBuilder,private router:Router,private authenticationservice:AuthenticationService,private userService:UserService) {
    if(this.authenticationservice.currentUserValue)
    {
      // redirect to home if already logged in
       this.router.navigate(['/']);
    }
   }

  ngOnInit() {
 this.registerform=this.formBuilder.group({
  email:['',Validators.required],
  UserName:['',Validators.required],
  password:['',[Validators.required,Validators.minLength(6)]],
  confirmPassword:['',[Validators.required,Validators.minLength(6)]]
},{
  validator: MustMatch('password', 'confirmPassword')
})



  }
    // convenience getter for easy access to form fields
  get f()
  {
    return this.registerform.controls;
  }


onSubmit(data){
  // console.log(JSON.stringify(data))
this.isSubmitted=true;

this.userService.register(data).subscribe(data=>{
  if(data['status'] == 401)
  // window.alert(data['message'])
  this.toastr.error(data['message'])
  // this.router.navigate(['/login']),{queryParms:{registered:true}}
},error=>
{
this.error=error;
this.loading=false;
})

}



}
