import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiService } from '../_services/apiservice.service';
import { apiUrls } from 'src/environments/apiUrls';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
  errorMessage: string;
  successMessage: string;
  IsvalidForm = false;
  forbiddenEmails: any;

  forgotform: FormGroup;
  data: any;
  constructor(private http: HttpClient, public router: Router,public fb: FormBuilder,public apiService: ApiService) { }

  ngOnInit() {
    this.forgotform = this.fb.group({
      femail: ['', [Validators.required, Validators.email],this.forbiddenEmails],
  });
  }

  get f(){return this.forgotform.controls;}
  forgotPwd()
  {
    this.IsvalidForm=true;
    if(this.forgotform.invalid)
    {
      return;
    }
this.apiService.get(apiUrls.getForgotEmail ).subscribe(data=>
  {
    this.data=data;
    if(this.data.statusCode == 200)
    {
      this.forgotform.reset();
      this.successMessage = "Reset password link send to email sucessfully.";
      setTimeout(() => {
        this.successMessage = null;
        this.router.navigate(['rest-password']);
      }, 3000);
    } err => {

      if (err.error.message) {
        this.errorMessage = err.error.message;
      }
    }
  })

  }
}
