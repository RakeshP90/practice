import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_home/home.component';
import { LoginComponent } from './_login/login/login.component';
import { AuthGuard} from './_helper/auth.guard';
import {RegisterComponent} from './register/register.component'
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import {RestpasswordComponent} from './restpassword/restpassword.component';
const routes: Routes = [{path:'home',component:HomeComponent ,canActivate: [AuthGuard]},
{path:'login' ,component:LoginComponent},
{path:'register' ,component:RegisterComponent},
{path:'forgot-password' ,component:ForgotpasswordComponent},
{path:'rest-password' ,component:RestpasswordComponent},
{path:"",redirectTo:'/login',pathMatch:"full"},
{path:"**",redirectTo:'/login',pathMatch:"full"}
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
