import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './_home/home.component';
import { LoginComponent } from './_login/login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { fakeBackendProvider } from './_helper/fake-backend';
import { ErrorInterceptor } from './_helper/error.interceptor';
import { JwtInterceptor} from './_helper/jwt.interceptor';
import { RegisterComponent } from './register/register.component';
import { ConfigService } from './_services/apiconf.service';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { RestpasswordComponent } from './restpassword/restpassword.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    RestpasswordComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularMultiSelectModule,
      BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot() ,
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:JwtInterceptor,multi:true},{provide:HTTP_INTERCEPTORS,useClass:ErrorInterceptor,multi:true},ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
