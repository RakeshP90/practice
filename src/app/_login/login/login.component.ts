import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm:FormGroup;
loading=false;
submitted=false;
returnUrl:string;
error='';
  constructor(private formBuilder:FormBuilder,private route:ActivatedRoute,private router:Router,private authenticationService:AuthenticationService) { 
if(this.authenticationService.currentUserValue)
{
  this.router.navigate(['/home']);
}

  }

  ngOnInit() {
    this.loginForm=this.formBuilder.group({
      username:['',Validators.required],
      password:['',Validators.required]
    });
    this.returnUrl=this.route.snapshot.queryParams['returnUrl'] || '/home';

  }

  get f(){return this.loginForm.controls;}

  onSubmit()
  {
    this.submitted=true;

    if(this.loginForm.invalid)
    {
      return;
    }
    // this.loading=true;
    this.authenticationService.login(this.loginForm.value).subscribe(data=>
      {
        console.log(data);
        if(data && data["status"] == 200)
        console.log(this.returnUrl)
        this.router.navigate([this.returnUrl]);
        // else
        // window.alert(data.message)
      },error=>
      {
        this.error=error;
        this.loading=false;
        window.alert(this.error);
      })

  }

}
