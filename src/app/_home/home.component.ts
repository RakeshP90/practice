import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { DataService } from '../_services/data.service';
import { ApiService } from '../_services/apiservice.service';
import { apiUrls } from 'src/environments/apiUrls';
import {environment} from 'src/environments/environment'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
loading=false;
users:User[];
userdropList:any=[];
multidroplist:any=[];
myDateValue: Date;
dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};
    selectMapping=[];

    addressProofObj = {
      proofName: '',
      proofDesc: '',
      selectedItems: '',
      proofStatus: 'true'
    };

  constructor(private userService:UserService,private dataService:DataService,private apiService:ApiService) { }

  ngOnInit() {
    // this.loading=true;
    this.myDateValue = new Date();
    this.userService.getAll().pipe(first()).subscribe(users=>{
      this.loading=false;
      this.users=users;
    })
    this.dropdownSettings = { 
      singleSelection: false, 
      text:"Select",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };    
    this.userdropdownlist();
    this.multiSelectlist();
  }

  userdropdownlist()
  {
this.apiService.get(environment.backendapiUrl + apiUrls.getuserdropdownlist).subscribe((response:any) =>
  {
    console.log(response);
    
    this.userdropList=response.users;
    console.log("userdroplist",this.userdropList);
    
  })
  }

multiSelectlist()
{
  this.apiService.get(environment.backendapiUrl + apiUrls.getMultiselectlist).subscribe((response:any)=>
  {
    this.multiSelectlist=response.multiprooflist.map(element => {
      return {
        itemName: element.multiseldropName,
        id: element.id
      }
    });
  })
}

onItemSelect(item: any) {
  // this.selectMapping.push({ id: item.id });
  this.selectMapping.push(item.id);
}
OnItemDeSelect(item: any) {
  let arrIdx = this.selectMapping.findIndex((data) => { return data.id == item.id });
  if (arrIdx > -1) {
    this.selectMapping.splice(arrIdx);
  }
}
onSelectAll(items: any) {
  for (let index = 0; index < items.length; index++) {
    const element = items[index];
    let arrIdx = this.selectMapping.findIndex((data) => { return data.id == element.id });
    if (arrIdx === -1) {
      this.selectMapping.push({ id: element.id });
    }
  }
}
onDeSelectAll(items: any) {
  this.selectMapping = [];
}

  onDateChange(newDate: Date) {
    console.log(newDate);
  }

//   onItemSelect(item:any){
//     console.log(item);
//     console.log(this.selectedItems);
// }
// OnItemDeSelect(item:any){
//     console.log(item);
//     console.log(this.selectedItems);
// }
// onSelectAll(items: any){
//     console.log(items);
// }
// onDeSelectAll(items: any){
//     console.log(items);
// }

}
